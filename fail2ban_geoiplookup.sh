#!/usr/bin/bash
#
# file: fail2ban_geoiplookup.sh
#
# gather all IP addresses from /var/log/fail2ban* logs
# then use geoiplookup to display each country for each banned ip
#
# must be run as root
#
# tested on Ubuntu 22.04
# 
# make sure geoiplookup command is installed and working with:
#   apt install geoip-bin
#
# Marc Compere, comperem@gmail.com
# created : 30 Jul 2023
# modified: 31 Jul 2023
#
# ---
# Copyright 2023 Marc Compere
#
# fail2ban_geoiplookup.sh is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License version 3
# as published by the Free Software Foundation.
#
# fail2ban_geoiplookup.sh is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License version 3 for more details.
#
# A copy of the GNU General Public License is included with MoVE
# in the COPYING file, or see <https://www.gnu.org/licenses/>.
# ---
echo "starting"



# pull all IP addresses from fail2ban logs:
# note: this is the only command that requires root access
zgrep 'Ban' /var/log/fail2ban.log* > /tmp/fail2banList.txt
# file: /tmp/fail2banList.txt
# looks like this:
#   /var/log/fail2ban.log:2023-07-30 17:56:57,443 fail2ban.actions        [97482]: NOTICE  [sshd] Ban 193.118.61.117
#   /var/log/fail2ban.log:2023-07-30 19:21:38,986 fail2ban.actions        [97482]: NOTICE  [sshd] Ban 164.90.228.206
#   /var/log/fail2ban.log:2023-07-30 19:45:02,784 fail2ban.actions        [97482]: NOTICE  [sshd] Ban 103.178.234.13
#   /var/log/fail2ban.log:2023-07-30 20:06:45,637 fail2ban.actions        [649]: NOTICE  [sshd] Restore Ban 1.235.198.20
#   /var/log/fail2ban.log:2023-07-30 20:06:45,880 fail2ban.actions        [649]: NOTICE  [sshd] Restore Ban 101.43.110.129
#   /var/log/fail2ban.log:2023-07-30 20:06:45,924 fail2ban.actions        [649]: NOTICE  [sshd] Restore Ban 102.128.78.77
#   .
#   .
#   .



# extract a new list with IPv4 addresses only
cat /tmp/fail2banList.txt |awk '{ if ($7=="Ban") print $8 } { if ($7=="Restore") print $9 }' > /tmp/fail2banIPs_only.txt

N_BAN_EVENTS=`cat /tmp/fail2banIPs_only.txt |wc -l`
echo "discovered $N_BAN_EVENTS ban occurrences"

# file: /tmp/fail2banIPs_only.txt
# looks like this:
#   193.118.61.117
#   164.90.228.206
#   103.178.234.13
#   1.235.198.20
#   101.43.110.129
#   102.128.78.77
#   .
#   .
#   .




cat /tmp/fail2banIPs_only.txt | sort -u > /tmp/fail2banIPs_only_sorted_unique.txt

N_BANNED_IPS=`cat /tmp/fail2banIPs_only_sorted_unique.txt |wc -l`
echo "discovered $N_BANNED_IPS unique banned ip addresses"




# loop through each IP address and do the geoiplookup
# from: https://stackoverflow.com/a/1521470/7621907
echo "starting geoiplookup of each IP!"
cat /tmp/fail2banIPs_only_sorted_unique.txt | while read line
do
   # do something with $line here
   RETVAL=`geoiplookup $line`
   printf '\nip: %-18s --> %s' $line "$RETVAL"
done
echo ""

# final output is like this:
#   GeoIP Country Edition: US, United States
#   GeoIP Country Edition: ES, Spain
#   GeoIP Country Edition: IP Address not found
#   GeoIP Country Edition: CN, China
#   GeoIP Country Edition: SG, Singapore
#   GeoIP Country Edition: VN, Vietnam


rm /tmp/fail2banList.txt
rm /tmp/fail2banIPs_only.txt
rm /tmp/fail2banIPs_only_sorted_unique.txt


