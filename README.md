# fail2ban_geoiplookup

This is a simple script that extracts banned IP addresses from [fail2ban](https://en.wikipedia.org/wiki/Fail2ban) logs and provides an estimate of where each IP address originates around the globe.

The commands are simple and can be tested individually. There are no confusing regular expressions.

Only the commands fail2ban-client, awk, [geoiplookup](https://manpages.ubuntu.com/manpages/trusty/man1/geoiplookup.1.html#description), and bash are used.

The script must be run as root for access to fail2ban logs in <code>/var/log</code>

Tested on Ubuntu 22.04
 
Make sure <code>geoiplookup</code> command is installed and working with:

    apt install geoip-bin

## Screenshot from bash command line
Here is a screenshot of this script running with expected output:
![screenshot](fail2ban_geoiplookup_screenshot.png)

## Contributing
If you are interested in improving this, let me know via email at comperem@gmail.com.


## License
This is free and open source software licensed under the GNU GPL_v3 license.

***
Marc Compere, comperem@gmail.com  
created : 30 Jul 2023  
modified: 30 Jul 2023
